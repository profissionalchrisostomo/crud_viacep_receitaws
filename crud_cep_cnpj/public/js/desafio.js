
document.getElementById('cnpj').addEventListener('input', function (e) {
    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,3})(\d{0,3})(\d{0,4})(\d{0,2})/);
    e.target.value = !x[2] ? x[1] : x[1] + '.' + x[2] + '.' + x[3] + '/' + x[4] + (x[5] ? '-' + x[5] : '');
});

document.getElementById('cep').addEventListener('input', function (e) {
    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,3})(\d{0,3})/);
    e.target.value = !x[2] ? x[1] : x[1] + '.' + x[2] + '-' + x[3] ;
});



$(document).ready(function () {
    $("#cpf_1,#cpf_2,#cpf_3,#cpf_4,#cpf_5").hide();
});

$("input[type=radio]").on("change", function () {
    if ($(this).val() == "pf") {
        $("#cpf_1,#cpf_2,#cpf_3,#cpf_4,#cpf_5").show();
        $("#cnpj_1,#cnpj_2,#cnpj_3,#cnpj_4,#cnpj_5,#cnpj_6,#cnpj_7,#cnpj_8,#cnpj_9").hide();
    } else if ($(this).val() == "pj") {
        $("#cnpj_1,#cnpj_2,#cnpj_3,#cnpj_4,#cnpj_5,#cnpj_6,#cnpj_7,#cnpj_8,#cnpj_9").show();
        $("#cpf_1,#cpf_2,#cpf_3,#cpf_4,#cpf_5").hide();
    }
});



function getCNPJ() {

    var cnpjObj = ({ "cnpj": $("#cnpj").val().replace(/[^\d]+/g, '') });

    var url = window.location;

    $.ajax({
        url: url.origin + '/buscar_cnpj',
        method: 'GET',
        data: { cnpj: cnpjObj.cnpj },
        // dataType: 'json',
        success: function (data) {
            dados = JSON.parse(data)
            $(document).ready(function () {
                $("input[name='razao_social']").val(dados['razao_social']);
                $("input[name='nome_fantasia']").val(dados['nome_fantasia']);
            })
        }
    });

}

function getCEP(){
    var cepObj = ({ "cep": $("#cep").val().replace(/[^\d]+/g, '') });

    var url = window.location

    $.ajax({
        url: url.origin + '/buscar',
        method: 'GET',
        data: { cnpj: cepObj.cnpj },
        // dataType: 'json',
        success: function (data) {
            dados = JSON.parse(data)
            console.log(dados)
            // $(document).ready(function () {
            //     $("input[name='razao_social']").val(dados['razao_social']);
            //     $("input[name='nome_fantasia']").val(dados['nome_fantasia']);
            // })
        }
    })
}

function getCPF() {

    var cnpjObj = ({ "cnpj": $("#cnpj").val().replace(/[^\d]+/g, '') });

    var url = window.location;

    $.ajax({
        url: url.origin + '/buscar_cnpj',
        method: 'GET',
        data: { cnpj: cnpjObj.cnpj },
        dataType: 'json',
        success: function (data) {
            console.log(data)
        }
    });

}

function addContatos() {
    $('#contato1').html('')
	$('#contato1').append('<div class="card-body" style="display: block;"><div class="row" id="contato1">  <div class="col-md-3"><div class="form-group"><label class="control-label">Telefone<span style="color: red;"><sup>•</sup></span></label><input type="text" id="tel" name="tel" class="form-control not-required-cnpj not-value rounded-form" data-step="2" required="" value=""></div></div><div class="col-sm-3"><div class="form-group"><label class="control-label">Tipo<span style="color: red;"><sup>•</sup></span></label><div class="select2-purple"><select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true"><option value="">Selecione</option><option value="residential">Residencial</option><option value="commercial">Comercial</option>    <option value="cellphone">Celular</option></select></div></div></div>  <div class="col-md-3"><div class="form-group"><label class="control-label">E-mail</label><input type="text" id="email" name="email" class="form-control not-required-cnpj not-value rounded-form" data-step="2" required="" value=""></div></div><div class="col-sm-3"><div class="form-group"><label class="control-label">Tipo</label><div class="select2-purple"><select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true"><option value="">Selecione</option><option value="personal">Pessoal</option><option value="commercial">Comercial</option><option value="other">Outro</option></select></div></div></div></div></div></div>&nbsp;&nbsp; <a href="#" onclick="remover()">Remover</a>')
}

function remover(){
    $('#contato1').html("<span style='color: #999;'>NÃO HÁ CONTATOS ADICIONAIS.</span>")
}
