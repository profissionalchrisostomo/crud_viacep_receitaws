@extends('adminlte::page')

@section('title', 'Cadastro Fornecedor')

@section('content_header')
<h1>Cadastro Fornecedor</h1>
@stop

@section('content')
<form action="{{route('salvar_cnpj')}}" method="POST" class="card-body">
    <div class="card-body" style="display: block;">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Dados do Fornecedor</h3>
                <div class="card-tools">
                    <button type="button pull-right" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body" style="display: block;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group" data-step="1"><label class="radio-inline">
                                <input type="radio" id="type_pj" name="type" class="client-type" value="pj" checked="">Pessoa Jurídica</label><label class="radio-inline"> &nbsp;&nbsp;
                                <input type="radio" id="type_pf" name="type" class="client-type" value="pf">Pessoa Física</label>
                        </div>
                    </div>
                    <div class="col-md-3" id="cnpj_1">
                        <div class="form-group">
                            <label class="control-label">CNPJ<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="cnpj" onblur="getCNPJ(this)" name="cnpj" class="form-control not-required-cnpj not-value rounded-form" data-step="2" maxlength="18" required="" value="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cpf_1">
                        <div class="form-group">
                            <label class="control-label">CPF<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="cnpj" name="cpf" class="form-control not-required-cnpj not-value rounded-form" data-step="2" maxlength="14" required="" value="">
                        </div>
                    </div>
                    <div class="col-md-6" id="cnpj_2">
                        <div class="form-group">
                            <label class="control-label">Razão Social<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="razao_social" name="razao_social" class="form-control not-required-cpf not-value" data-step="2" required="" value="">
                        </div>
                    </div>
                    <div class="col-md-6" id="cpf_2">
                        <div class="form-group">
                            <label class="control-label">Nome<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="nome" name="nome" class="form-control not-required-cpf not-value" data-step="2" required="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cnpj_3">
                        <div class="form-group">
                            <label class="control-label">Nome Fantasia<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="nome_fantasia" name="nome_fantasia" class="form-control not-required-cpf not-value" data-step="2" required="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cpf_3">
                        <div class="form-group">
                            <label class="control-label">Apelido<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="apelido" name="apelido" class="form-control not-required-cpf not-value" data-step="2" required="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cpf_3">
                        <div class="form-group">
                            <label class="control-label">RG<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="rg" name="rg" class="form-control not-required-cpf not-value" data-step="2" required="">
                        </div>
                    </div>
                    <div class="col-sm-3" id="cnpj_4">
                        <div class="form-group">
                            <label class="control-label">Indicador de Inscrição Estadual<span style="color: red;"><sup>•</sup></span></label>
                            <div class="select2-purple">
                                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option value="">Selecione</option>
                                    <option value="1">Contribuinte</option>
                                    <option value="2">Contribuinte Isento</option>
                                    <option value="3">Não Contribuinte</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" id="cnpj_5">
                        <div class="form-group">
                            <label class="control-label">Inscrição Estadual<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" name="inscricao_estadual" id="incricao_estadual" class="form-control not-value" required="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cnpj_6">
                        <div class="form-group">
                            <label class="control-label">Inscrição Municipal<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" name="inscricao_municipal" id="incricao_municipal" class="form-control not-value" required="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cnpj_7">
                        <div class="form-group">
                            <label class="control-label">Situação CPNJ<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" name="inscricao_municipal" id="incricao_municipal" class="form-control not-value" required="">
                        </div>
                    </div>
                    <div class="col-sm-6" id="cnpj_8">
                        <div class="form-group">
                            <label class="control-label">Indicador de Inscrição Estadual<span style="color: red;"><sup>•</sup></span></label>
                            <div class="select2-purple">
                                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option value="">Selecione</option>
                                    <option value="recolher">A Recolher pelo Prestador</option>
                                    <option value="retido">Retido pelo Tomador</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6" id="cnpj_9">
                        <div class="form-group">
                            <label class="control-label">Ativo<span style="color: red;"><sup>•</sup></span></label>
                            <div class="select2-purple">
                                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option value="">Selecione</option>
                                    <option value="0">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6" id="cpf_5">
                        <div class="form-group">
                            <label class="control-label">Ativo<span style="color: red;"><sup>•</sup></span></label>
                            <div class="select2-purple">
                                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option value="">Selecione</option>
                                    <option value="0">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body" style="display: block;">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Contato Principal</h3>
                <div class="card-tools">
                    <button type="button pull-right" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body" style="display: block;">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Telefone<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="tel" name="tel" class="form-control not-required-cnpj not-value rounded-form" data-step="2" required="" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Tipo<span style="color: red;"><sup>•</sup></span></label>
                            <div class="select2-purple">
                                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option value="">Selecione</option>
                                    <option value="residential">Residencial</option>
                                    <option value="commercial">Comercial</option>
                                    <option value="cellphone">Celular</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">E-mail</label>
                            <input type="text" id="email" name="email" class="form-control not-required-cnpj not-value rounded-form" data-step="2" required="" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Tipo</label>
                            <div class="select2-purple">
                                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option value="">Selecione</option>
                                    <option value="personal">Pessoal</option>
                                    <option value="commercial">Comercial</option>
                                    <option value="other">Outro</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a style="align-items: left;" href="#" onclick="addContatos()">Adicionar</a>
    <div class="card-body" style="display: block;">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Contato Adicional</h3>
                <div class="card-tools">
                    <button type="button pull-right" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body" style="display: block;">
                <div class="row" id="contato1">
                    <span style="align-items: left;" style="color: #999;">NÃO HÁ CONTATOS ADICIONAIS.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body" style="display: block;">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Dados do Endereço</h3>
                <div class="card-tools">
                    <button type="button pull-right" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body" style="display: block;">
                <div class="row">
                    <div class="col-md-3" id="cep">
                        <div class="form-group">
                            <label class="control-label">CEP<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="cep" onblur="getCEP(this)" name="cep" class="form-control not-required-cnpj not-value rounded-form" data-step="2" maxlength="10" required="" value="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cep">
                        <div class="form-group">
                            <label class="control-label">Lagradouro<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="lagradouro" name="lagradouro" class="form-control not-required-cnpj not-value rounded-form" data-step="2" maxlength="20" required="" value="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cep">
                        <div class="form-group">
                            <label class="control-label">Número<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="numero" name="numero" class="form-control not-required-cnpj not-value rounded-form" data-step="2" maxlength="10" required="" value="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cep">
                        <div class="form-group">
                            <label class="control-label">Complemento<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="comp" name="comp" class="form-control not-required-cnpj not-value rounded-form" data-step="2" required="" value="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cep">
                        <div class="form-group">
                            <label class="control-label">Bairro<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="bairro" name="bairro" class="form-control not-required-cnpj not-value rounded-form" data-step="2" required="" value="">
                        </div>
                    </div>
                    <div class="col-md-3" id="cep">
                        <div class="form-group">
                            <label class="control-label">Ponto de Referência<span style="color: red;"><sup>•</sup></span></label>
                            <input type="text" id="p_refe" name="p_refe" class="form-control not-required-cnpj not-value rounded-form" data-step="2" required="" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">UF<span style="color: red;"><sup>•</sup></span></label>
                            <select class="form-control select2 state select2-hidden-accessible" required="" style="width: 100%" data-step="4" tabindex="-1" aria-hidden="true">
                                <option value="">Selecione</option>
                                <option value="1">AC</option>
                                <option value="2">AL</option>
                                <option value="4">AM</option>
                                <option value="3">AP</option>
                                <option value="5">BA</option>
                                <option value="6">CE</option>
                                <option value="7">DF</option>
                                <option value="8">ES</option>
                                <option value="9">GO</option>
                                <option value="10">MA</option>
                                <option value="13">MG</option>
                                <option value="12">MS</option>
                                <option value="11">MT</option>
                                <option value="14">PA</option>
                                <option value="15">PB</option>
                                <option value="17">PE</option>
                                <option value="18">PI</option>
                                <option value="16">PR</option>
                                <option value="19">RJ</option>
                                <option value="20">RN</option>
                                <option value="22">RO</option>
                                <option value="23">RR</option>
                                <option value="21">RS</option>
                                <option value="24">SC</option>
                                <option value="26">SE</option>
                                <option value="25">SP</option>
                                <option value="27">TO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Cidade<span style="color: red;"><sup>•</sup></span></label>
                            <select name="city" class="form-control select2 city select2-hidden-accessible" required="" disabled="" style="width: 100%" data-step="5" tabindex="-1" aria-hidden="true">
                                <option value="">Selecione</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 div_cond">
                        <div class="form-group"><label class="control-label">Condomínio?<span style="color: red;"><sup>•</sup></span></label><select class="form-control select_cond" name="condominio" style="width: 100%" required="">
                                <option value="" selected="">Selecione</option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button id="save" type="submit"  class="btn btn-sm btn-success" data-step="10" data-intro="Clique neste botão para realizar seu cadastro do Fornecedor."><i class="fa fa-plus fa-fw"></i> Cadastrar</button>
</form>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script src="<?php echo asset('js/desafio.js') ?>" type="text/javascript">
</script>
@stop